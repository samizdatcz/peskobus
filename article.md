---
title: "Mapa: Jde to i bez lítačky. Podívejte se, kde budete pěšky rychleji než metrem"
perex: "Má v Praze smysl místo metra vyrazit do cíle pěšky? Spočítali jsme, jak dlouho trvá procházka mezi jednotlivými stanicemi. Na vzdálenost jedné zastávky je zejména v centru doba chůze a jízdy často srovnatelná."
description: "Má v Praze smysl místo metra vyrazit do cíle pěšky? Spočítali jsme, jak dlouho trvá procházka mezi jednotlivými stanicemi. Na vzdálenost jedné zastávky je zejména v centru doba chůze a jízdy často srovnatelná."
authors: ["Jan Cibulka", "Michal Zlatkovský"]
published: "9. března 2017"
socialimg: https://interaktivni.rozhlas.cz/data/peskobus/www/media/metro_chuze.jpg
#coverimg: https://interaktivni.rozhlas.cz/data/peskobus/www/media/cover.jpg
# coverimg_note: "Foto <a href='#'>ČTK</a>"
url: "metro-chuze"
# libraries: [d3, topojson, jquery, highcharts, leaflet, inline-audio]
recommended:
  - link: https://interaktivni.rozhlas.cz/rozbite-silnice/
    title: Mapa: Kolik nehod zaviní zanedbané silnice?
    perex: Český rozhlas analyzoval údaje z policejní databáze dopravních nehod. Podívejte se, kolik havárií způsobí „závada komunikace“, a zapojte se do hlasování o nejhorší výmol v republice.
    image: https://interaktivni.rozhlas.cz/rozbite-silnice/media/mapa.png
  - link: https://interaktivni.rozhlas.cz/olympijsky-beh/
    title: Jak se liší trénovaná běžkyně od amatérky? Olympijský běh změřily senzory
    perex: Svaly profesionálního běžce jsou uvyklé dlouhodobé námaze, díky rovnovážnému prostředí v organismu je pak tělo takového sportovce méně stresované, než je tomu u amatéra. Rozdíly ukázala analýza dat z desetikilometrového závodu.
    image: http://media.rozhlas.cz/_obrazek/3410633--tmobile-olympijsky-beh--1-950x0p0.jpeg
---

<div data-bso="1"></div>

Příkladem, kdy se pěší cesta může vyrovnat jízdě metrem, je úsek mezi Můstkem a Národní třídou. Metro mezi dvěma stanicemi ve středu města jede jen minutu - jenže k ní je nutné připočítat cestu tunely a případné několikaminutové čekání na další spoj. Středně rychlá pěší chůze mezi vstupy do metra přitom trvá osm minut.  

<aside class="big" style="text-align: right">
<figure>
<img src="media/metro_chuze.jpg" style="max-width: 100%">
</figure>
<figcaption>
<i>Mapa ukazuje dobu v minutách, za kterou lze středně rychlou chůzí ujít cestu mezi vstupy do jednotlivých stanic metra.<br> Počítáme přitom s tempem 100 kroků za minutu a průměrnou délkou kroku 0,78 metru. Rychlou chůzí lze časy zkrátit o více než třetinu.</i>
</figcaption>
</aside>

<aside class="small">
  <h3>
    Závod od koně k labuti
  </h3>
  <figure>
    <audio src="https://samizdat.blob.core.windows.net/storage/0309-repo-metro%20nebo%20p%C4%9B%C5%A1ky.mp3" controls="controls"></audio>
  </figure>
  <figcaption>
    Je cesta od Národního muzea na náměstí Republiky rychlejší metrem, nebo pěšky? Vyzkoušeli jsme to s reportérem Radiožurnálu Ľubomírem Smatanou. <i>Plánek je odvozený z <a href="http://www.dpp.cz/dopravni-schemata/">dopravních schémat DPP</a>.</i>
  </figcaption>
</aside> 

Cestu metrem může výrazně zpomalit i přestup mezi linkami. Vyzkoušeli jsme si to na vlastní kůži: v našem závodu mezi stanicemi Muzeum a Náměstí Republiky dorazil reportér Radiožurnálu, který cestu absolovoval pěšky, s dvouminutovým předstihem.

Mapa vychází z [podobného plánku pro Londýn](http://content.tfl.gov.uk/walking-tube-map.pdf), který v Londýně připravil dopravní podnik. Ten cestu pěšky doporučuje hlavně v ranních a odpoledních špičkách. V Česku pěší cesty po městech doporučuje iniciativa [Jezdím pro Brno](https://magazin.aktualne.cz/revolucni-peskobus-brnenska-iniciativa-pesky-pro-brno-vydava/r~9c8d385c8a1011e68d00002590604f2e/?redirected=1488990957), která cestující hromadnou dopravou láká na více pohybu a zdraví.


<aside class="big"><iframe src="https://interaktivni.rozhlas.cz/data/metro-chuze-mapa/" height="550" frameborder="0" style="max-height: 90wh"></iframe></aside>

_Informace o zastávkách a trasách metra pochází z [otevřených dat Institutu plánování a rozvoje](http://www.geoportalpraha.cz/cs/opendata), časy chůze mezi vstupy do zastávek (proto se pěší trasy neprotínají přesně se stanicemi metra) jsou vypočtené pomocí [Mapy.cz](https://mapy.cz)._

Extrémní variantu závodu s metrem zase v Praze v posledních letech zkusili běžci, kteří si dali za cíl vystoupit na jedné stanici a metro předběhnout tak, aby na další zastávce nastoupili do stejného vozu. Tyto pokusy ale vycházejí jen vzácně - třeba při dopravních výlukách.

<p style="position:relative;height:0;padding-bottom:56.25%"><iframe src="https://www.youtube.com/embed/phlpntYkmBo?rel=0&amp;controls=0?ecver=2" width="640" height="360" frameborder="0" style="position:absolute;width:100%;left:0" allowfullscreen></iframe></p>